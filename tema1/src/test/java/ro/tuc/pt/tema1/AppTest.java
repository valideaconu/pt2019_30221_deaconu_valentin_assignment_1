package ro.tuc.pt.tema1;

import ro.tuc.pt.tema1.Math.Polynomial;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    		assertTrue(checkAdding(	"x^2 + 2x + 1", 
    								"x^3 + 5x + 3", 
    								"x^3 + x^2 + 7x + 4"));

    		assertTrue(checkAdding(	"5x^3 + 2x^2 + x + 4", 
									"4x^2 - 2x + 3", 
									"5x^3 + 6x^2 - 1x + 7"));

			assertFalse(checkAdding("x^2 + 2x + 1", 
									"x^3 + 5x + 3", 
									"10x^3 + x^2 + 7x + 4"));

			assertTrue(checkSubstracting(	"x^2 + 2x + 1", 
											"x^3 + 5x + 3",
											"-1x^3 + 1x^2 - 3x - 2"));

			assertTrue(checkSubstracting(	"5x^3 + 2x^2 + x + 4", 
											"4x^2 - 2x + 3",
											"5x^3 - 2x^2 + 3x + 1"));
		
			assertFalse(checkSubstracting(	"x^2 + 2x + 1",
											"x^3 + 5x + 3", 
											"-x^3 + 1x^2 - 3x + 2"));

			assertTrue(checkMultiplication(	"x^2 + 2x + 1", 
											"x^3 + 5x + 3", 
											"x^5 + 2x^4 + 6x^3 + 13x^2 + 11x + 3"));

			assertTrue(checkMultiplication(	"5x^3 + 2x^2 + x + 4", 
											"4x^2 - 2x + 3", 
											"20x^5 - 2x^4 + 15x^3 + 20x^2 - 5x + 12"));

			assertTrue(checkMultiplication(	"13x^7 + 12x^5 + 5x^3 + 2x^2 + x + 4", 
											"14x^6 + 7x^3 + 4x^2 - 2x + 3", 
											"182x^13 + 168x^11 + 91x^10 + 122x^9 + 86x^8 + 101x^7 + 67x^6 + 70x^5 + 5x^4 + 43x^3 + 20x^2 - 5x + 12"));

			assertTrue(checkDivision(	"13x",
										"13",
										"x",
										"0"));

			assertTrue(checkDivision(	"x^2 + 3x + 2",
										"x + 1",
										"x + 2",
										"0"));

			assertTrue(checkDivision(	"x^2 + 2x + 1",
										"5x",
										"0.2x + 0.4",
										"1"));

			assertTrue(checkDerivative(	"4x^3 + 2",
										1,
										"12x^2"));

			assertFalse(checkDerivative("13x^7 + 12x^5 + 5x^3 + 2x^2 + x + 4",
										4,
										"5x^3 + 2x^2 + x + 4"));

			assertTrue(checkDerivative(	"13x^7 + 12x^5 + 5x^3 + 2x^2 + x + 4",
										2,
										"546x^5 + 240x^3 + 30x + 4"));

			assertTrue(checkIntegrate(	"5x^3 + 3x^2 + x + 4",
										"1.25x^4 + 1x^3 + 0.5x^2 + 4x"));

			assertTrue(checkIntegrate(	"x^5 + 2x^4 + 6x^3 + 13x^2 + 11x + 3",
										"0.1666x^6 + 0.4x^5 + 1.5x^4 + 4.3333x^3 + 5.5x^2 + 3x"));

			assertTrue(checkIntegrate(	"-x^3 + 1x^2 - 3x + 2",
										"-0.25x^4 + 0.3333x^3 - 1.5x^2 + 2x"));

			assertTrue(checkIntegrateWithHeads(	"-x^3 + 1x^2 - 3x + 2",
												0,
												1,
												0.5833));

			assertTrue(checkIntegrateWithHeads(	"5x^3 + 3x^2 + x + 4",
												0,
												Math.PI,
												170.268));

    }

	private static boolean checkAdding(String pol1, String pol2, String result) {
		Polynomial p1 = new Polynomial(pol1);
		Polynomial p2 = new Polynomial(pol2);
		
		Polynomial res = new Polynomial(result);
		
		if (p1.add(p2).equals(res))
			return true;
		
		return false;
	}
	
	private static boolean checkSubstracting(String pol1, String pol2, String result) {
		Polynomial p1 = new Polynomial(pol1);
		Polynomial p2 = new Polynomial(pol2);
		
		Polynomial res = new Polynomial(result);
		
		if (p1.sub(p2).equals(res))
			return true;
		
		return false;
	}
	
	private static boolean checkMultiplication(String pol1, String pol2, String result) {
		Polynomial p1 = new Polynomial(pol1);
		Polynomial p2 = new Polynomial(pol2);
		
		Polynomial res = new Polynomial(result);
		
		if (p1.multiply(p2).equals(res))
			return true;
		
		return false;
	}
	
	private static boolean checkDivision(String pol1, String pol2, String quotient, String reminder) {
		Polynomial p1 = new Polynomial(pol1);
		Polynomial p2 = new Polynomial(pol2);
		
		Polynomial qu = new Polynomial(quotient);
		Polynomial rm = new Polynomial(reminder);
		
		Polynomial[] res = p1.divide(p2);	
		
		if (res[0].equals(qu) && res[1].equals(rm))
			return true;
		
		return false;
	}
	
	private static boolean checkDerivative(String pol1, int order, String result) {
		Polynomial p1 = new Polynomial(pol1);
		
		Polynomial res = new Polynomial(result);
		
		if (p1.derivative(order).equals(res))
			return true;
		
		return false;
	}
	

	private static boolean checkIntegrate(String pol1, String result) {
		Polynomial p1 = new Polynomial(pol1);
		
		Polynomial res = new Polynomial(result);
		
		if (p1.integrate().equals(res))
			return true;
		
		return false;
	}
	
	private static boolean checkIntegrateWithHeads(String pol1, double lowerHead, double upperHead, double result) {
		final double EPSILON = 0.001;
		
		Polynomial p1 = new Polynomial(pol1);
		
		if (Math.abs(p1.integrate(lowerHead, upperHead) - result) < EPSILON)
			return true;
		
		return false;
	}
    
}
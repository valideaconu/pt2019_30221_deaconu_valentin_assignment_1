package ro.tuc.pt.tema1.UI;

import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class FooterPanel extends JPanel {
	private static final long serialVersionUID = -5726890117752143772L;

	public FooterPanel() {
		this.setLayout(new FlowLayout());
		
		JLabel lblInfo = new JLabel("Tema 1 Tehnici de Programare - Valentin Deaconu");
		lblInfo.setHorizontalAlignment(SwingConstants.CENTER);
		lblInfo.setFont(MainFrame.fontUsed);
		
		this.add(lblInfo);
	}
	
}

package ro.tuc.pt.tema1.UI;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 912613475253900500L;

	public static void main(String[] args) {
		new MainFrame();
	}
	
	protected final static Font fontUsed = new Font("Ubuntu", Font.PLAIN, 16);
	protected JTextField polynomial1Tf;
	protected JTextField polynomial2Tf;
	protected JEditorPane resultEp;
	
	public MainFrame() {
		this.setTitle("Polynomial processing system");
		this.setSize(900, 400);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new BorderLayout());

		this.add(new CenterPanel(this), BorderLayout.CENTER);
		this.add(new ButtonsPanel(this), BorderLayout.LINE_END);
		this.add(new FooterPanel(), BorderLayout.PAGE_END);		
		
		this.setVisible(true);
	}
}

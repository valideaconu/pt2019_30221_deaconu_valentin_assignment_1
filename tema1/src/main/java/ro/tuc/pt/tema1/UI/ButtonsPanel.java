package ro.tuc.pt.tema1.UI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import ro.tuc.pt.tema1.Math.Polynomial;

public class ButtonsPanel extends JPanel {
	private static final long serialVersionUID = 5359728553840172200L;
	
	public ButtonsPanel(final MainFrame window) {	
		this.setLayout(new GridLayout(0, 1));

		// List of buttons available only if both polynomials exists
		final ArrayList<JButton> bothPolynomials = new ArrayList<JButton>();
		// List of buttons available only if first polynomial exists
		final ArrayList<JButton> firstPolynomial = new ArrayList<JButton>();
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				Polynomial p2 = new Polynomial(window.polynomial2Tf.getText());
				
				window.resultEp.setText(p1.add(p2).toString());
			}
		});
		this.add(btnAdd);
		bothPolynomials.add(btnAdd);
		
		JButton btnSubstract = new JButton("Substract");
		btnSubstract.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				Polynomial p2 = new Polynomial(window.polynomial2Tf.getText());
				
				window.resultEp.setText(p1.sub(p2).toString());
			}
		});
		this.add(btnSubstract);
		bothPolynomials.add(btnSubstract);
		

		JButton btnMultiply = new JButton("Multiply");
		btnMultiply.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				Polynomial p2 = new Polynomial(window.polynomial2Tf.getText());
				
				window.resultEp.setText(p1.multiply(p2).toString());
			}
		});
		this.add(btnMultiply);
		bothPolynomials.add(btnMultiply);
		

		JButton btnDivide = new JButton("Divide");
		btnDivide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				Polynomial p2 = new Polynomial(window.polynomial2Tf.getText());
				
				if (!p2.isZero()) {				
					if (p1.degree() < p2.degree()) {
						JOptionPane.showMessageDialog(window, "First polynomial's degree is lower than second's!", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						Polynomial[] res = p1.divide(p2);
						
						String txt = new String("");
						txt += "Quotient: " + res[0].toString() + "<br/>Reminder: " + res[1].toString(); 
						
						window.resultEp.setText(txt);
					}
				} else {
					JOptionPane.showMessageDialog(window, "Second polynomial cannot be zero!", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		this.add(btnDivide);
		bothPolynomials.add(btnDivide);
		
		
		JButton btnDerivative = new JButton("Derivative");
		btnDerivative.setToolTipText("Operation for Polynomial #1.");
		btnDerivative.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				
				String input =  JOptionPane.showInputDialog(window, "Insert derivative order:");
				
				int result = 0;
				try {
					result = Integer.parseInt(input);
					window.resultEp.setText(p1.derivative(result).toString());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(window, "Bad input!", "Error", JOptionPane.ERROR_MESSAGE);
				}				
			}
		});
		this.add(btnDerivative);
		firstPolynomial.add(btnDerivative);
		
		JButton btnIntegrate = new JButton("Integrate");
		btnIntegrate.setToolTipText("Operation for Polynomial #1.");
		btnIntegrate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				
				window.resultEp.setText(p1.integrate().toString());
			}
		});
		this.add(btnIntegrate);
		firstPolynomial.add(btnIntegrate);
		
		JButton btnIntegrateHeads = new JButton("Integrate with heads");
		btnIntegrateHeads.setToolTipText("Operation for Polynomial #1.");
		btnIntegrateHeads.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Polynomial p1 = new Polynomial(window.polynomial1Tf.getText());
				
				String inputLower = JOptionPane.showInputDialog(window, "Insert lower head: ");
				double lowerHead = 0.0;
				try {
					lowerHead = Double.parseDouble(inputLower);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(window, "Bad input!", "Error", JOptionPane.ERROR_MESSAGE);					
				}
				
				String inputUpper = JOptionPane.showInputDialog(window, "Insert upper head: ");
				double upperHead = 0.0;
				try {
					upperHead = Double.parseDouble(inputUpper);
					window.resultEp.setText(p1.integrate(lowerHead, upperHead) + "");
				} catch(Exception e) {
					JOptionPane.showMessageDialog(window, "Bad input!", "Error", JOptionPane.ERROR_MESSAGE);	
				}
			}
		});
		this.add(btnIntegrateHeads);
		firstPolynomial.add(btnIntegrateHeads);
		
		// Buttons activators
		for (JButton b : firstPolynomial)
			b.setEnabled(false);
		for (JButton b : bothPolynomials)
			b.setEnabled(false);
		
		window.polynomial1Tf.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {								
			}

			public void insertUpdate(DocumentEvent e) {
				for (JButton b : firstPolynomial)
					b.setEnabled(true);
				
				if (!window.polynomial2Tf.getText().equals(""))
					for (JButton b : bothPolynomials)
						b.setEnabled(true);
			}

			public void removeUpdate(DocumentEvent e) {
				if (window.polynomial1Tf.getText().equals("")) {
					for (JButton b : bothPolynomials)
						b.setEnabled(false);
					
					for (JButton b : firstPolynomial)
						b.setEnabled(false);
				}
			}
		});

		window.polynomial2Tf.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {					
			}

			public void insertUpdate(DocumentEvent e) {
				for (JButton b : bothPolynomials)
					b.setEnabled(true);
			}

			public void removeUpdate(DocumentEvent e) {
				if (window.polynomial2Tf.getText().equals("")) {
					for (JButton b : bothPolynomials)
						b.setEnabled(false);
				}
			}
		});
	}
}

package ro.tuc.pt.tema1.Math;

import javax.swing.JOptionPane;

// @author Vali

public class Monomial implements Comparable<Monomial> {
	double coefficient;
	int power;
	
	// Older constructor, with more than 40 lines
	/* public Monomial(String input) {
		Scanner scan = new Scanner(input);
		
		try {
			if (input.contains("x^")) { // if contains "x^" we have coefficient and power
				if (input.startsWith("x^")) {
					this.coefficient = 1;
					scan.useDelimiter("x\\^");
					this.power = scan.nextInt();
				} else if (input.startsWith("-x^")) {
					this.coefficient = -1;
					scan.useDelimiter("\\-x\\^");
					this.power = scan.nextInt();
				} else {
					scan.useDelimiter("x\\^");
					this.coefficient = scan.nextDouble();
					this.power = scan.nextInt();
				}
			} else if (input.contains("x")) { // if contains "x" we have coefficient and set power to 1
				if (input.startsWith("x")) {
					this.coefficient = 1;
					this.power = 1;
				} else if (input.startsWith("-x")) { 
					this.coefficient = -1;
					this.power = 1;
				} else {
					scan.useDelimiter("x");
					if (scan.hasNext()) {
						this.coefficient = scan.nextDouble();
					} else {
						this.coefficient = 1;
					}
					this.power = 1;
				}
			} else { 
				// if neither of above, we have only coefficient and set power to 0
				if (scan.hasNext()) {
					this.coefficient = scan.nextDouble();
				} else {
					this.coefficient = 0;
					this.power = 0;
				}
			}
		} catch (InputMismatchException e) {
			System.out.println(input);
			JOptionPane.showMessageDialog(null, "Bad input!", "Error", JOptionPane.ERROR_MESSAGE);	
			
			this.coefficient = 0;
			this.power = 0;
		}
		
		scan.close();
	}*/
	
	// Building a monom based on a string like "5x^2"
	public Monomial(String input) {
		try {
			if (input.contains("x^")) {
				String r[] = input.split("x\\^");
				if (r.length == 0 || r[0] == null || r[0].equals(""))
					this.coefficient = 1.0f;
				else if (r[0].equals("-"))
					this.coefficient = -1.0f;
				else
					this.coefficient = Double.parseDouble(r[0]);
				this.power = Integer.parseInt(r[1]);
			} else if (input.contains("x")) {
				String r[] = input.split("x");
				if (r.length == 0 || r[0] == null || r[0].equals(""))
					this.coefficient = 1.0f;
				else if (r[0].equals("-"))
					this.coefficient = -1.0f;
				else
					this.coefficient = Double.parseDouble(r[0]);
				this.power = 1;
			} else {
				if (input == null || input.equals(""))
					this.coefficient = 0;
				else
					this.coefficient = Double.parseDouble(input);
				this.power = 0;
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Bad input!", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public Monomial(double coef, int pow) {
		this.coefficient = coef;
		this.power = pow;
	}
	
	public Monomial() {
		coefficient = 0;
		power = 0;
	}
	
	public double getCoefficent() {
		return coefficient;
	}
	
	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}
	
	public int getPower() {
		return power;
	}
	
	public void setPower(int power) {
		this.power = power;
	}
	
	// toString method
	public String toString() {
		final double EPSILON = 0.0001f;
		String str = new String();
		if (Math.abs(coefficient - 0) < EPSILON) 
			return new String("0");
		else if (Math.abs(coefficient - 1) < EPSILON) {
			if (power == 0)
				str += "1.0";
		} else if (Math.abs(coefficient + 1) < EPSILON)
			str += "-";
		else
			str += coefficient + "";
		
		if (power == 0)
			str += "";
		else if (power == 1)
			str += "x";
		else
			str += "x^" + power;
		
		return str;
	}

	public Monomial copy() {
		Monomial m = new Monomial();
		m.coefficient = this.coefficient;
		m.power = this.power;
		return m;
	}
	
	public Monomial multiply(Monomial m) {
		Monomial c = this.copy();
		
		c.coefficient *= m.coefficient;
		c.power += m.power;
		
		return c;
	}
	
	public int compareTo(Monomial m) {
		final double EPSILON = 0.001;
		if (this.power > m.power) {
			return -1;
		} else if (this.power < m.power) {
			return 1;
		} else {
			if (Math.abs(this.coefficient - m.coefficient) < EPSILON)
				return 0;
			
			if (this.coefficient > m.coefficient) {
				return -1;
				
			} else if (this.coefficient < m.coefficient) {
				return 1;
			}
		}
		
		return 0;
	}
}

package ro.tuc.pt.tema1.Math;

import java.util.ArrayList;
import java.util.Collections;

// @author Vali

public class Polynomial {
	private ArrayList<Monomial> monoms;
	
	private boolean isIntegrated;

	// Building a polynomial based on a string with multiple monomials like "5x^2+3x+2"
	public Polynomial(String input) {
		monoms = new ArrayList<Monomial>();
		this.isIntegrated = false;
		
		if (input.contains(" ")) {
			input = input.replace(" ", "");
		}
		
		if (input.equals("")) {
			monoms.add(new Monomial());
		} else {
			input = input.replace("-", "+-");
			
			for (String monomString : input.split("\\+")) {
				monoms.add(new Monomial(monomString));
			}
			
			this.organize();
		}
	}
	
	public Polynomial() {
		monoms = new ArrayList<Monomial>();
		isIntegrated = false;
	}
	
	// Searching for monomials with same power and collide them into one
	// Sorting monomial list by their powers
	public void organize() {
		for (int i = 0; i < monoms.size(); ++i) {
			if (monoms.get(i).getCoefficent() == 0.0) {
				monoms.remove(i);
				--i;
			}
		}
		
		for (int i = 0; i < monoms.size() - 1; ++i) {
			Monomial m = monoms.get(i);
			for (int j = i + 1; j < monoms.size(); ++j) {
				Monomial p = monoms.get(j);
				if (m.getPower() == p.getPower()) {
					m.setCoefficient(m.getCoefficent() + p.getCoefficent());
					monoms.remove(j);
					--j;
				}
			}
		}
		
		Collections.sort(monoms);
	}
	
	// Checking if current polynomial is zero
	public boolean isZero() {
		this.organize();
		
		if (monoms != null && monoms.size() != 0) {
			for (Monomial m : monoms)
				if (m.getCoefficent() != 0)
					return false;	
			return true;
		}			
		return true;
	}
	
	// toString method
	@Override
	public String toString() {
		if (monoms == null || monoms.size() == 0)
			return new String("0");
		
		String str = new String(monoms.get(0).toString() + " ");
		
		Polynomial p = this.copy();
		for (int i = 1; i < p.monoms.size(); ++i) {
			if (p.monoms.get(i).getCoefficent() > 0) {
				str += "+ " + p.monoms.get(i).toString() + " ";
			} else {
				p.monoms.get(i).setCoefficient((-1) * p.monoms.get(i).getCoefficent());
				str += "- " + p.monoms.get(i).toString() + " ";
			}
		}
		
		if (p.isIntegrated)
			str += "+ C";
		
		return str;
	}
	
	// Method for making a copy of a polynomial
	public Polynomial copy() {
		Polynomial p = new Polynomial();
		
		for (Monomial m : this.monoms) {
			p.monoms.add(m.copy());
		}
		
		p.isIntegrated = this.isIntegrated;
		
		this.organize();
		
		return p;
	}
	
	// Method for computing polynomial's degree
	public int degree() {
		if (monoms.size() == 0)
			return Integer.MIN_VALUE;
		return monoms.get(0).getPower();
	}
	
	// Method for computing polynomial's leading coefficient
	public double leadingCoefficient() {
		if (monoms.size() == 0)
			return Integer.MIN_VALUE;
		
		return monoms.get(0).getCoefficent();
	}
	
	// Method for adding a polynomial to current polynomial
	public Polynomial add(Polynomial p) {
		Polynomial n = this.copy();
		
		for (Monomial m : p.monoms) {
			n.monoms.add(m);
		}
		
		n.organize();
		
		return n;
	}

	// Method for substracting a polynomial from current polynomial
	public Polynomial sub(Polynomial p) {
		Polynomial n = this.copy();

		for (Monomial m : p.monoms) {
			m.setCoefficient(m.getCoefficent() * (-1));
			n.monoms.add(m);
		}
		
		n.organize();
		
		return n;
	}
	
	// Method for multipling a polynomial with the current polynomial
	public Polynomial multiply(Polynomial p) {
		Polynomial n = new Polynomial();
		
		for (Monomial m : this.monoms) {
			for (Monomial m1 : p.monoms) {
				n.monoms.add(m.multiply(m1));
			}
		}
		
		n.organize();
		return n;
	}
	
	// Method for division of 2 polynomials, using Euclid's algorithm
	public Polynomial[] divide(Polynomial p) {		
		Polynomial q = new Polynomial(); // result
		Polynomial r = this.copy();
		int d = p.degree();
		double c = p.leadingCoefficient();
		
		while (r.degree() >= d) {
			Monomial m = new Monomial(r.leadingCoefficient() / c, r.degree() - d);
			q.monoms.add(m);
			
			Polynomial t = new Polynomial();
			t.monoms.add(m);
			
			r = r.sub(p.multiply(t));
		}
		
		q.organize();
		r.organize();
		return new Polynomial[] { q, r };
	}
	
	// Method to find a derivative of a degree of our polynomial
	public Polynomial derivative(int degree) {
		Polynomial n = this.copy();
		
		if (degree <= 0)
			return n;
		
		for (int k = 0; k < degree; ++k) {
			for (int i = 0; i < n.monoms.size(); ++i) {
				Monomial m = n.monoms.get(i);
				
				if (m.getPower() >= 1) {
					m.setCoefficient(m.getCoefficent() * m.getPower());
					m.setPower(m.getPower() - 1);
				} else if (m.getPower() == 0) {
					n.monoms.remove(i);
					--i;
				}
			}
		}
		
		if (n.monoms.size() == 0) {
			n.monoms.add(new Monomial());
		}
		
		return n;
	}
	
	// Method to integrate our polynomial
	public Polynomial integrate() {	
		Polynomial n = this.copy();
			
		for (Monomial m : n.monoms) {
			m.setPower(m.getPower() + 1);
			if (m.getPower() != 0) {
				m.setCoefficient(m.getCoefficent() / m.getPower());
			}
		}
		
		n.isIntegrated = true;
		
		return n;
	}
	
	// Method to calculate integrate of our polynomial between two heads of an interval
	public double integrate(double a, double b) {
		Polynomial r = this.integrate();
		
		double result = 0.0;
		for (Monomial m : r.monoms) {
			result += (m.getCoefficent() * (Math.pow(b, m.getPower()))) 
					- (m.getCoefficent() * (Math.pow(a, m.getPower())));
		}
	
		return result;
	}

	// equals method
	public boolean equals(Polynomial p) {
		this.organize();
		p.organize();
		
		if (this.monoms.size() > p.monoms.size()) {
			return false;
		} else {
			for (int i = 0; i < this.monoms.size(); ++i) {
				if (this.monoms.get(i).compareTo(p.monoms.get(i)) != 0) {
					return false;
				}
			}
		}
		
		return true;
	}
}

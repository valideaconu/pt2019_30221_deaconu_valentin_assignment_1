package ro.tuc.pt.tema1.UI;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.TextArea;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;

public class CenterPanel extends JPanel {
	private static final long serialVersionUID = -6257437945721490534L;

	public CenterPanel(MainFrame window) {
		this.setLayout(new GridLayout(0, 2));

		JLabel polynomial1Lbl = new JLabel("Polynomial #1");
		polynomial1Lbl.setFont(MainFrame.fontUsed);
		polynomial1Lbl.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(polynomial1Lbl);
		
		window.polynomial1Tf = new JTextField();
		window.polynomial1Tf.setFont(MainFrame.fontUsed);
		window.polynomial1Tf.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(window.polynomial1Tf);
		
		JLabel polynomial2Lbl = new JLabel("Polynomial #2");
		polynomial2Lbl.setFont(MainFrame.fontUsed);
		polynomial2Lbl.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(polynomial2Lbl);

		window.polynomial2Tf = new JTextField();
		window.polynomial2Tf.setFont(MainFrame.fontUsed);
		window.polynomial2Tf.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(window.polynomial2Tf);
		
		JLabel resultLbl = new JLabel("Result");
		resultLbl.setFont(MainFrame.fontUsed);
		resultLbl.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(resultLbl);
		
		window.resultEp = new JEditorPane() {
			private static final long serialVersionUID = 1L;

			@Override
			public void setText(String text) {
				String newText = new String();
				newText += "<html>";
				newText += "<div "
						+ "align='center' "
						+ "style='font-family: Ubuntu; "
						+ "font-size: 12px; "
						+ "'>";
				newText += text;
				newText += "</div>";
				super.setText(newText);
			}
		};
		window.resultEp.setEditable(false);
		window.resultEp.setContentType("text/html");
		window.resultEp.setBorder(new MatteBorder(1, 1, 1, 1, Color.GRAY));
		window.resultEp.setFont(MainFrame.fontUsed);
		window.resultEp.setAlignmentX(TextArea.CENTER_ALIGNMENT);
		JScrollPane resultSp = new JScrollPane(window.resultEp);
		this.add(resultSp);
	}
}
